package space.schober.adapternachten;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class HTML {
	private String path;
	private Document doc;
	private MimeMultipart multipart;

	public HTML(String path) {
		this.setPath(path);
		multipart = new MimeMultipart("related");
	}

	public void generateHTML(Person receiver) {
		try {

			MimeBodyPart messageBodyPart;

			// Add HTML
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(insertDataintoHTML(doc.toString(), receiver), "UTF-8", "html");
			this.multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private String insertDataintoHTML(String html, Person receiver) {
		return html.replace("%RECEIVER%", receiver.getLinkedPerson().getName().getCompleteName());
	}

	public String getCSS() {
		StringBuilder stb = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader(String.format("%s/css/main.css", this.getPath())))) {

			while (br.ready()) {
				stb.append(br.readLine());
				stb.append("\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return stb.toString();
	}

	public String getPath() {
		return this.path;
	}

	public MimeMultipart getMultipart() {
		return this.multipart;
	}

	public void setPath(String path) {
		this.path = path;
		try {
			doc = Jsoup.parse(new File(String.format("%s/index.html", this.getPath())), "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
